<?php
/**
 * @category Bitbull_OrderExport
 * @package  Bitbull
 * @author   Mirko Cesaro <mirko.cesaro@bitbull.it>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$connection = $installer->getConnection();

$installer->startSetup();

$tableName = $installer->getTable('bitbull_order_export/exported_order');

$table= $connection->newTable($tableName)
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'ID')
    ->addForeignKey(
        $installer->getFkName(
            $tableName,
            'id',
            $installer->getTable('sales/order'),
            'entity_id'),
        'id',
        $installer->getTable('sales/order'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    );

$connection->createTable($table);

$installer->endSetup();
