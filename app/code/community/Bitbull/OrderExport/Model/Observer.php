<?php
/**
 * @category Bitbull
 * @package  Bitbull_OrderExport
 * @author   Mirko Cesaro <mirko.cesaro@bitbull.it>
 */

class Bitbull_OrderExport_Model_Observer
{
    /*
     * @todo usare logger
     * */
    public function afterOrderSave(Varien_Event_Observer $event)
    {
        Mage::log('-----------------------------------', Zend_Log::DEBUG, 'Bitbull_OrderExport.log');
        Mage::log(__METHOD__ . ' called.', Zend_Log::DEBUG, 'Bitbull_OrderExport.log');
        /** @var Mage_Sales_Model_Order $order */
        $order = $event->getOrder();
        Mage::log('Increment Id: '.$order->getIncrementId() .' Order Id: '. $order->getId(), Zend_Log::DEBUG, 'Bitbull_OrderExport.log');

        /** @var Bitbull_OrderExport_Helper_Data $helper */
        $helper = Mage::helper('bitbull_order_export');

        $stateStatusString = $order->getState() . '::' . $order->getStatus();
        $isOrderAlreadyExported = $helper->isOrderAlreadyExported($order);
        $paymentMethodCode = $order->getPayment()->getMethod();
        $exportOrderStatusForPaymentMethod = $helper->getExportOrderStatusForPaymentMethod($paymentMethodCode);
        Mage::log('Order state-status string: ' . $stateStatusString, Zend_Log::DEBUG, 'Bitbull_OrderExport.log');
        Mage::log('Is order already exported: ' . ($isOrderAlreadyExported ?'yes':'no'), Zend_Log::DEBUG, 'Bitbull_OrderExport.log');
        Mage::log('Order payment method code: ' . $paymentMethodCode, Zend_Log::DEBUG, 'Bitbull_OrderExport.log');
        Mage::log(
            'Configured export order status for payment method: ' . $exportOrderStatusForPaymentMethod,
            Zend_Log::DEBUG,
            'Bitbull_OrderExport.log'
        );

        if ($stateStatusString === $exportOrderStatusForPaymentMethod && !$isOrderAlreadyExported) {
            Mage::log('Exporting order with ID ' . $order->getId(), Zend_Log::DEBUG, 'Bitbull_OrderExport.log');
            $helper->exportOrders(array($order));
            $helper->markOrderAsAlreadyExported($order);
            Mage::log('Exported order with ID ' . $order->getId(), Zend_Log::DEBUG, 'Bitbull_OrderExport.log');
        }
    }
}
