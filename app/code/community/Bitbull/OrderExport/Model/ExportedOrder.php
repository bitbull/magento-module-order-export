<?php
/**
 * @category Bitbull
 * @package  Bitbull_OrderExport
 * @author   Mirko Cesaro <mirko.cesaro@bitbull.it>
 */

class Bitbull_OrderExport_Model_ExportedOrder extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('bitbull_order_export/exportedOrder');
    }
}
