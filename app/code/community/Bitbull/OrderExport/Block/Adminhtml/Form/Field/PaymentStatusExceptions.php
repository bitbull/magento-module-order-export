<?php
/**
 * @category Bitbull
 * @package  Bitbull_OrderExport
 * @author   Mirko Cesaro <mirko.cesaro@bitbull.it>
 */

class Bitbull_OrderExport_Block_Adminhtml_Form_Field_PaymentStatusExceptions extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    private $_paymentMethodRenderer;
    private $_exportOrderStatusRenderer;

    protected function _prepareToRender()
    {
        $this->addColumn(
            'payment_method',
            array(
                'label' => Mage::helper('adminhtml')->__('Payment Method'),
                'renderer' => $this->_getPaymentMethodRenderer(),
            )
        );
        $this->addColumn(
            'export_order_status',
            array(
                'label' => Mage::helper('adminhtml')->__('Export Order Status'),
                'renderer' => $this->_getSuccessfulOrderStatusesRenderer(),
            )
        );

        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('adminhtml')->__('Add Exception');
    }

    /**
     * @return Bitbull_OrderExport_Block_Adminhtml_Form_Field_SelectRenderer
     */
    private function _getSuccessfulOrderStatusesRenderer()
    {
        if ($this->_exportOrderStatusRenderer) {
            return $this->_exportOrderStatusRenderer;
        }

        $this->_exportOrderStatusRenderer = $this->_createOrderStatusesRenderer();
        return $this->_exportOrderStatusRenderer;
    }

    /**
     * @return Bitbull_OrderExport_Block_Adminhtml_Form_Field_SelectRenderer
     */
    private function _createOrderStatusesRenderer()
    {
        /** @var Bitbull_OrderExport_Block_Adminhtml_Form_Field_SelectRenderer $renderer */
        $renderer = $this->getLayout()->createBlock(
            'bitbull_order_export/adminhtml_form_field_selectRenderer',
            '',
            array('is_render_to_js_template' => true)
        );
        $options = array_merge(
            array(array('value' => '', 'label' => Mage::helper('adminhtml')->__('-- Please Select --'))),
            Mage::getModel('bitbull_order_export/system_config_source_order_stateStatus')->toOptionArray()
        );
        $renderer->setOptions($options);
        $renderer->setExtraParams('style="width:200px"');
        return $renderer;
    }

    /**
     * @return Bitbull_OrderExport_Block_Adminhtml_Form_Field_SelectRenderer
     */
    private function _getPaymentMethodRenderer()
    {
        if ($this->_paymentMethodRenderer) {
            return $this->_paymentMethodRenderer;
        }

        $this->_paymentMethodRenderer = $this->getLayout()->createBlock(
            'bitbull_order_export/adminhtml_form_field_selectRenderer',
            '',
            array('is_render_to_js_template' => true)
        );
        $options = array_merge(
            array(array('value' => '', 'label' => Mage::helper('adminhtml')->__('-- Please Select --'))),
            Mage::getSingleton('adminhtml/system_config_source_payment_allmethods')->toOptionArray()
        );
        $this->_paymentMethodRenderer->setOptions($options);
        $this->_paymentMethodRenderer->setExtraParams('style="width:300px"');
        return $this->_paymentMethodRenderer;
    }

    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->_getPaymentMethodRenderer()->calcOptionHash($row->getData('payment_method')),
            'selected="selected"'
        );
        $row->setData(
            'option_extra_attr_' . $this->_getSuccessfulOrderStatusesRenderer()->calcOptionHash(
                $row->getData('export_order_status')
            ),
            'selected="selected"'
        );
    }
}