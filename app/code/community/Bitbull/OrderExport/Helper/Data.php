<?php
/**
 * @category Bitbull
 * @package  Bitbull_OrderExport
 * @author   Mirko Cesaro <mirko.cesaro@bitbull.it>
 */

class Bitbull_OrderExport_Helper_Data extends Mage_Core_Helper_Abstract
{
    const CSV_DELIMITER = ';';

    const PRODUCT_TYPE = '1';

    const DOCUMENT_TYPE_ORDER = 1;
    const DOCUMENT_TYPE_REPLACEMENT = 2;

    const NOT_INVOICED_REQUEST = 0;
    const INVOICED_REQUEST = 1;

    const PAYMENT_CODE_BNL = 'BNL';
    const PAYMENT_CODE_PAYPAL = 'PAYPAL';
    const PAYMENT_CODE_BANKTRANSFER = 'BANKTRANSFER';
    const PAYMENT_CODE_CASHONDELIVERY = 'CASHONDELIVERY';
    const PAYMENT_CODE_CHECKMO = 'CHECKMO';
    const PAYMENT_CODE_PURCHASEORDER = 'PURCHASEORDER';
    const PAYMENT_CODE_OTHER = 'OTHER';

    const LIMIT_NOTE = 250;
    const LIMIT_NOTE_ADDRESS = 80;

    private $_lengthOrder, $_lengthOrderItem;

    /**
     * @var array
     */
    private $toDelete = array();
    private $exportedFiles = array();

    function __construct()
    {
        $this->_createLengthArray();
    }

    public function exportOrders(array $orders)
    {
        try {
            foreach ($this->getNewExportData($orders) as $fileName => $data) {
                $path = $this->createFile($fileName,$data);
                if(file_exists($path)){
                    if(!is_readable($path)){
                        Mage::log('File not readable: '.$path, Zend_Log::ERR, 'Bitbull_OrderExport.log');
                    }
                }else{
                    Mage::log('File not exist: '.$path, Zend_Log::ERR, 'Bitbull_OrderExport.log');
                }

            }
        } catch (Exception $ex) {
            Mage::log(
                'Exception while exporting orders! Trace:' . PHP_EOL . $ex->getTraceAsString(),
                Zend_Log::CRIT,
                'Bitbull_OrderExport.log'
            );

            //Cancella tutti i file (incluso lo zip)
            $this->cleanExportedFilesByError();

            //Rilancia
            throw $ex;
        }

        return $this->exportedFiles;
    }

    public function exportZipOrders(array $orders)
    {

        $fileName = $this->getZipNameByOrders($orders);

        $zip = $this->createZipFile($fileName);

        $tempDir = rtrim(sys_get_temp_dir(), DS).DS;
        try {
            foreach ($this->getNewExportData($orders) as $fileName => $data) {

                $path =$this->createTemporaryFile($tempDir, $fileName, $data);
                if(!$zip->addFile($path, $fileName)){
                    //if file wasn't add to zip so we log details
                    Mage::log('file temp: '.$path, Zend_Log::DEBUG, 'Bitbull_OrderExport.log');
                    if(file_exists($path)){
                        if(!is_readable($path)){
                            Mage::log('File not readable: '.$path, Zend_Log::ERR, 'Bitbull_OrderExport.log');
                        }
                    }else{
                        Mage::log('File not exist: '.$path, Zend_Log::ERR, 'Bitbull_OrderExport.log');
                    }
                }
            }
        } catch (Exception $ex) {
            Mage::log(
                'Exception while exporting orders! Trace:' . PHP_EOL . $ex->getTraceAsString(),
                Zend_Log::CRIT,
                'Bitbull_OrderExport.log'
            );

            //Cancella tutti i file (incluso lo zip)
            $zipFilePath = $this->cleanup($zip);
            @unlink($zipFilePath);

            //Rilancia
            throw $ex;
        }

        return $this->cleanup($zip);
    }

    public function getOrdersExportData(array $orders)
    {
        $keys = array_keys($this->_lengthOrder);
        $result = array($keys);

        /** @var Mage_Sales_Model_Order $order */
        foreach ($orders as $order) {
            if ($order->getState() !== 'holded') {
                $shippingAddress = $order->getShippingAddress();

                $tipoOrdine = $this->getDocumentType($order);
                $result[] = array(
                    $this->_cutStringToLimitOrder($tipoOrdine,'Tipo Documento'),
                    $this->_cutStringToLimitOrder($order->getIncrementId(), 'Id Ordine') ,
                    $this->_cutStringToLimitOrder(Mage::getModel('core/date')->date('Y-m-d H:i', $order->getCreatedAtDate()),'Data Ordine'),
                    $this->formatNumber($order->getGrandTotal()),
                    $this->formatNumber($order->getTaxAmount()),
                    $this->formatNumber($order->getDiscountAmount()),
                    $this->_cutStringToLimitOrder($tipoOrdine == self::DOCUMENT_TYPE_REPLACEMENT ? 'Rif.to Id Ordine precedente':'','Rif.to Id Ordine'),
                    $this->_cutStringToLimitOrder($this->getNote($order),'Note'),
                    $this->_cutStringToLimitOrder($order->getCustomerId(),'User_Id Magento'),
                    '',
                    $this->_cutStringToLimitOrder($order->getBillingAddress()->getFirstname(),'User_Nome'),
                    $this->_cutStringToLimitOrder($order->getBillingAddress()->getLastname(),'User_Cognome'),
                    $this->_cutStringToLimitOrder($order->getBillingAddress()->getStreet1(),'User_Indirizzo'),
                    $this->_cutStringToLimitOrder($order->getBillingAddress()->getStreet2()?:'','User_Indirizzo 2'),
                    $this->_cutStringToLimitOrder($order->getBillingAddress()->getPostcode(),'User_CAP'),
                    $this->_cutStringToLimitOrder($order->getBillingAddress()->getCity(),'User_Citta'),
                    $this->_cutStringToLimitOrder($order->getBillingAddress()->getRegionCode()?:'','User_Provincia'),
                    $this->_cutStringToLimitOrder($order->getBillingAddress()->getCountryId(),'User_Nazione'),
                    $this->_cutStringToLimitOrder($order->getBillingAddress()->getTelephone()?:'','User_Telefono'),
                    '',//User_Cellulare
                    $this->_cutStringToLimitOrder($order->getCustomerEmail(),'User_e-mail'),
                    $this->_cutStringToLimitOrder($shippingAddress ? $shippingAddress->getFirstname() :'','Ship-to_Nome'),
                    $this->_cutStringToLimitOrder($shippingAddress ? $shippingAddress->getLastname():'','Ship-to_Nome 2'),
                    $this->_cutStringToLimitOrder($shippingAddress ? $shippingAddress->getStreet1() : '','Ship-to_Indirizzo'),
                    $this->_cutStringToLimitOrder($shippingAddress ? $shippingAddress->getStreet2() : '','Ship-to_Indirizzo 2'),
                    $this->_cutStringToLimitOrder($shippingAddress ? $shippingAddress->getPostcode() : '','Ship-to_CAP'),
                    $this->_cutStringToLimitOrder($shippingAddress ? $shippingAddress->getCity() : '','Ship-to_Citta'),
                    $this->_cutStringToLimitOrder($shippingAddress ? $shippingAddress->getRegionCode() : '','Ship-to_Provincia'),
                    $this->_cutStringToLimitOrder($shippingAddress ? $shippingAddress->getCountryId() : '','Ship-to_Nazione'),
                    '',
                    $this->_cutStringToLimitOrder($shippingAddress ? $shippingAddress->getTelephone() : '','Ship-to_Telefono'),
                    '',//Ship-to_Note
                    $this->getInvoicedRequest($order),
                    $this->_cutStringToLimitOrder($order->getBillingAddress()->getFirstname(),'Bill-to_Nome'),
                    $this->_cutStringToLimitOrder($order->getBillingAddress()->getLastname(),'Bill-to_Nome 2'),
                    $this->_cutStringToLimitOrder($order->getBillingAddress()->getStreet1(),'Bill-to_Indirizzo'),
                    $this->_cutStringToLimitOrder($order->getBillingAddress()->getStreet2()?:'','Bill-to_Indirizzo 2'),
                    $this->_cutStringToLimitOrder($order->getBillingAddress()->getPostcode(),'Bill-to_CAP'),
                    $this->_cutStringToLimitOrder($order->getBillingAddress()->getCity(),'Bill-to_Citta'),
                    $this->_cutStringToLimitOrder($order->getBillingAddress()->getRegionCode()?:'','Bill-to_Provincia'),
                    $this->_cutStringToLimitOrder($order->getBillingAddress()->getCountryId(),'Bill-to_Nazione'),
                    '',
                    $this->_cutStringToLimitOrder($order->getBillingAddress()->getTelephone()?:'','Bill-to_Telefono'),
                    '',//Bill-to_Note
                    '',//Partita IVA
                    '',//Codice Fiscale
                    $this->_cutStringToLimitOrder($this->getPaymentType($order),'Metodo Pagamento'),
                    '',//Codice Conferma Pagamento
                    $this->_cutStringToLimitOrder($order->getPayment()!==false ? $order->getPayment()->getLastTransId() : '','Numero Transazione'), //Numero Transazione
                    $this->_cutStringToLimitOrder($order->getCouponCode(),'Coupon')//codice coupon
                );
            }
        }
        return $result;
    }

    public function getOrderItemsExportData(array $orders)
    {
        $keys = array_keys($this->_lengthOrderItem);
        $result = array( $keys );

        /** @var Mage_Sales_Model_Order $order */
        foreach ($orders as $order) {
            /** @var Mage_Sales_Model_Order_Item $item */
            if ($order->getState() !== 'holded') {
                $documentType = $this->getDocumentType($order);
                $i = 1;
                foreach ($order->getAllItems() as $item) {
                    $children = $item->getChildrenItems();
                    $product = Mage::getModel('catalog/product')->load($item->getProductId());
                    if(!$product->getId())
                    {
                        $product = false;
                    }
                    if (empty ($children)) {
                        $parent = $item;
                        while ($parent->getParentItem()) {
                            $parent = $parent->getParentItem();
                        }
                        $result[] = array(
                            $documentType,
                            $this->_cutStringToLimitOrderItem($item->getOrder()->getIncrementId(),'Id Ordine' ),
                            $i++,
                            self::PRODUCT_TYPE,//articolo
                            $this->_cutStringToLimitOrderItem($item->getSku(),'Codice articolo'),
                            $this->_cutStringToLimitOrderItem($item->getName(),'Descrizione'),
                            $this->formatNumber($item->getQtyOrdered()),
                            $this->formatNumber($parent->getPriceInclTax()),
                            $this->formatNumber($parent->getRowTotalInclTax()),
                            $this->formatNumber($parent->getTaxAmount()),
                            $this->formatNumber($parent->getRowTotalInclTax()),
                            '',//Rif.to Riga Ordine
                        );
                    }
                }
            }
        }

        return $result;
    }

    public function getOrderCustomersExportData(array $orders)
    {
        $result = array(
            array(
                'CODICE',
                'DESCRIZIONE',
                'VIA',
                'CAP',
                'PROVINCIA',
                'CITTA',
                'NAZIONE',
                'TELEFONO',
                'PARTITA_IVA',
                'EMAIL'
            )
        );

        /** @var Mage_Sales_Model_Order $order */
        foreach ($orders as $order) {
            if ($order->getState() !== 'holded') {

                $result[] = array(
                    $this->getCustomerCodeForExport($order->getCustomerId()),
                    $order->getBillingAddress()->getCompany() ?: $order->getCustomerName(),
                    $order->getBillingAddress()->getStreetFull(),
                    $order->getBillingAddress()->getPostcode(),
                    $order->getBillingAddress()->getRegionCode(),
                    $order->getBillingAddress()->getCity(),
                    $order->getBillingAddress()->getCountryId(),
                    $order->getBillingAddress()->getTelephone(),
                    $order->getCustomerTaxvat(),
                    $order->getBillingAddress()->getEmail()
                );
            }
        }

        return $result;
    }

    public function getExportOrderStatusForPaymentMethod($paymentMethodCode)
    {
        $exceptions = unserialize(Mage::getStoreConfig('bitbull_export/export_settings/exceptions'));

        if ($exceptions && array_key_exists($paymentMethodCode, $exceptions)) {
            return $exceptions[$paymentMethodCode]['export_order_status'];
        }

        return Mage::getStoreConfig('bitbull_export/export_settings/default_export_order_status');
    }

    public function isOrderAlreadyExported(Mage_Sales_Model_Order $order)
    {
        if (Mage::getModel('bitbull_order_export/exportedOrder')->load($order->getId())->getId()) {
            return true;
        }

        return false;
    }

    public function markOrderAsAlreadyExported(Mage_Sales_Model_Order $order)
    {
        $exportedOrder = Mage::getModel('bitbull_order_export/exportedOrder')->load($order->getId());

        if ($exportedOrder->getId()) {
            return;
        }

        $exportedOrder->setId($order->getId())->save();
    }

    /**
     * @return mixed|string
     */
    public function getExportPath()
    {
        return rtrim(Mage::getStoreConfig('bitbull_export/export_settings/export_path'), DS) . DS;
    }

    /**
     * @return ZipArchive
     * @throws Exception
     */
    private function createZipFile($nameOfFile)
    {
        $zipFileName = $this->getExportPath().$nameOfFile . '_' .date('Ymd_Hi').'.ZIP';
        $zip = new ZipArchive();
        if ($zip->open($zipFileName, ZipArchive::CREATE | ZipArchive::EXCL) !== true) {
            throw new LogicException('Export zip file already exists');
        }
        return $zip;
    }

    /**
     * @param array $orders
     * @return array
     */
    private function getNewExportData(array $orders)
    {
        $baseFilesName = $this->getBaseFilesName($orders);
        return array(
            $baseFilesName . '-order.CSV' => $this->getOrdersExportData($orders),
            $baseFilesName . '-items.CSV' => $this->getOrderItemsExportData($orders),
        );
    }

    protected function getBaseFilesName($orders){

        $ordersIds = array();
        foreach ($orders as $order){
            $ordersIds[] = $order->getIncrementId();
        }
        return implode('-',$ordersIds);
    }


    /**
     * @param $tempDir
     * @param $fileName
     * @param $data
     * @return string
     */
    private function createTemporaryFile($tempDir, $fileName, $data)
    {
        $tempFileName = tempnam($tempDir, 'export' . $fileName);
        $this->toDelete[] = $tempFileName;
        $file = fopen($tempFileName, 'w+');
        $this->writeDataToFile($data, $file);
        fclose($file);
        return $tempFileName;
    }

    /**
     * Method to create a csv file in exportPath with the data of the order
     * @param $fileName
     * @param $data
     * @return string name with path of the created file
     */
    protected function createFile($fileName, $data){
        $tempFileName = $this->getExportPath() . $fileName;
        $this->exportedFiles[] = $tempFileName;
        $file = fopen($tempFileName, 'w+');
        $this->writeDataToFile($data, $file);
        fclose($file);
        return $tempFileName;
    }

    /**
     * @param array $data
     * @param $file
     */
    private function writeDataToFile(array $data, $file)
    {
        //Scrivi sul file
        ftruncate($file, 0);
        rewind($file);
        foreach ($data as $row) {
            fputcsv($file, $row, self::CSV_DELIMITER);
        }
    }

    /**
     * @param ZipArchive $zip
     * @return string
     */
    private function cleanup(ZipArchive $zip)
    {
        $zipPath = $zip->filename;
        if ( !$zip->close() ) {
            Mage::log(
                'Impossible to close the zip file: '.$zip->filename . PHP_EOL . $zip->getStatusString(),
                Zend_Log::ERR,
                'Bitbull_OrderExport.log'
            );
        }
        foreach ($this->toDelete as $filePath) {
            @unlink($filePath);
        }
        $this->toDelete = array();
        return $zipPath;
    }

    /**
     * Method to delete exported files after an error
     */
    protected function cleanExportedFilesByError(){
        foreach ($this->exportedFiles as $filePath) {
            @unlink($filePath);
        }
    }

    /**
     * @param $number
     * @return string
     */
    private function formatNumber($number)
    {
        //4 decimals and . as separator
        return number_format($number, 4, '.', '');
    }

    /**
     * Method that create the note string to export with all customizable options
     * @param Mage_Sales_Model_Order_Item $item
     * @return string
     */
    protected function getNoteForItem($item)
    {
        /*
            _ (underscore) per separare sku dell'item e label della custom option
            : (due punti) per separare label e valore della custom option
            - (trattino) per separare custom option diverse dello stesso item
         */
        $prodOptions=$item->getProductOptions();
        $result = '';
        if(array_key_exists('options',$prodOptions)){
            $result = $item->getSku().'_';
            $options= array();
            foreach ($prodOptions['options'] as $value) {
                //create the string with label:value
                $option = $this->formatStringToNote($value['label']).':';
                $option .= $this->formatStringToNote($value['print_value']);
                array_push($options,$option);
            }
            //implode all options
            $result .= implode('-',$options);
        }
        return $result;
    }

    /**
     * Method to add slash in string for ' " and \ and excape the separators of the note
     * @param string $value
     * @return string
     */
    protected function formatStringToNote( $value ) {

        //Quote string with slashes
        $value = addslashes($value);
        //escape of -
        $value = str_replace( '-', '\-', $value );
        //escape of :
        $value = str_replace( ':', '\:', $value );
        return $value;
    }

    /**
     * Method to retrieve the notes for the order
     * @param Mage_Sales_Model_Order $order
     * @return string
     */
    protected function getNote(Mage_Sales_Model_Order $order)
    {
        /*
            | (pipe) per separare item diversi
        */

        $items = $order->getAllVisibleItems();
        $result = array();
        foreach($items as $item){
            array_push($result ,$this->getNoteForItem($item));
        }
        $result = array_filter( $result);

        $result  = implode('|', $result);
        /** @var Mage_Core_Helper_String $stringHelper */
        $stringHelper = Mage::helper('core/string');
        $result = $stringHelper->substr($result,0, self::LIMIT_NOTE);

        return $result;
    }

    /**
     * Method to retrive the payment type of the order
     * @todo fire event to let other modules return payment type
     *
     * @param Mage_Sales_Model_Order $order
     * @return null|string
     */
    protected function getPaymentType($order)
    {
        $paymentCode = null;
        $payment = $order->getPayment();

        switch ($payment->getMethod()){

            case Mage_Paypal_Model_Config::METHOD_WPS:
            case Mage_Paypal_Model_Config::METHOD_WPP_EXPRESS:
            case Mage_Paypal_Model_Config::METHOD_WPP_DIRECT:
            case Mage_Paypal_Model_Config::METHOD_WPP_PE_DIRECT:
            case Mage_Paypal_Model_Config::METHOD_WPP_PE_EXPRESS:
                $paymentCode = self::PAYMENT_CODE_PAYPAL;
                break;
            case Mage_Payment_Model_Method_Banktransfer::PAYMENT_METHOD_BANKTRANSFER_CODE:
                $paymentCode = self::PAYMENT_CODE_BANKTRANSFER;
                break;
            case 'cashondelivery': //Mage_Payment_Model_Method_Cashondelivery
                $paymentCode = self::PAYMENT_CODE_CASHONDELIVERY;
                break;
            case 'checkmo': //Mage_Payment_Model_Method_Checkmo
                $paymentCode = self::PAYMENT_CODE_CHECKMO;
                break;
            case 'purchaseorder': //Mage_Payment_Model_Method_Purchaseorder
                $paymentCode = self::PAYMENT_CODE_PURCHASEORDER;
                break;
            default:
                $paymentCode = self::PAYMENT_CODE_OTHER;
        }
        return $paymentCode;
    }

    protected function getZipNameByOrders($orders)
    {
        $filename = '';
        foreach($orders as $order){
            $filename .=$order->getIncrementId();
        }
        return $filename;
    }

    protected function getDocumentType($order){
        //TODO: scegliere come impostare la sostituzione
        return self::DOCUMENT_TYPE_ORDER;
    }

    protected function getInvoicedRequest($order){
        //TODO: aggiungere modulo per la richiesta della fattura
        return self::NOT_INVOICED_REQUEST;
    }

    protected function _cutStringToLimit($string, $type, $arrayType){
        $array= $arrayType =='ORDER'? $this->_lengthOrder: $this->_lengthOrderItem;

        if($type != null && array_key_exists($type, $array) && $array[$type] != null  ){
            $string = substr($string, 0, $array[$type]);
        }
        return $string;
    }

    protected function _cutStringToLimitOrder($string, $type){
        return $this->_cutStringToLimit($string,$type, 'ORDER');
    }
    protected function _cutStringToLimitOrderItem($string, $type){
        return $this->_cutStringToLimit($string,$type, 'ORDER_ITEM');
    }

    protected function _createLengthArray()
    {
        $this->_lengthOrder = array(
            'Tipo Documento' => null,
            'Id Ordine' => 20,
            'Data Ordine'=> null,
            'Importo Totale Pagato'=> null,
            'Importo IVA'=> null,
            'Importo Sconto Testata'=> null,
            'Rif.to Id Ordine' => 20,
            'Note' => 250,
            'User_Id Magento' => 20,
            'User_Titolo' => 20,
            'User_Nome' => 30,
            'User_Cognome'=> 30,
            'User_Indirizzo'=> 50,
            'User_Indirizzo 2'=> 50,
            'User_CAP'=> 20,
            'User_Citta' => 30,
            'User_Provincia' => 10,
            'User_Nazione' => 30,
            'User_Telefono'=> 30,
            'User_Cellulare'=> 30,
            'User_e-mail'=> 80,
            'Ship-to_Nome'=> 50,
            'Ship-to_Nome 2'=> 50,
            'Ship-to_Indirizzo'=> 50,
            'Ship-to_Indirizzo 2' => 50,
            'Ship-to_CAP'=> 20,
            'Ship-to_Citta' => 30,
            'Ship-to_Provincia' => 10,
            'Ship-to_Nazione' => 30,
            'Ship-to_Contatto'=> 50,
            'Ship-to_Telefono' => 30,
            'Ship-to_Note' => 80,
            'Richiesta Fattura'=> null,
            'Bill-to_Nome' => 50,
            'Bill-to_Nome 2' => 50,
            'Bill-to_Indirizzo' => 50,
            'Bill-to_Indirizzo 2' => 50,
            'Bill-to_CAP' => 20,
            'Bill-to_Citta' => 30,
            'Bill-to_Provincia' => 10,
            'Bill-to_Nazione'  => 30,
            'Bill-to_Contatto' => 50,
            'Bill-to_Telefono' => 30,
            'Bill-to_Note' => 80,
            'Partita IVA' => 20,
            'Codice Fiscale'  => 20,
            'Metodo Pagamento' => 50,
            'Codice Conferma Pagamento' => 50,
            'Numero Transazione' => 30,
            'Coupon' => 30
        );

        $this->_lengthOrderItem = array(
            'Tipo Documento'=> null,
            'Id Ordine' => 20,
            'Nr. Riga Ordine'=> null,
            'Tipo'=> null,
            'Codice articolo'=> 20,
            'Descrizione' => 50,
            'Quantità'=> null,
            'Imponibile Unitario'=> null,
            'Imponibile Totale'=> null,
            'Importo IVA'=> null,
            'Importo Pagato'=> null,
            'Rif.to Riga Ordine'=> null
        );
    }

}
